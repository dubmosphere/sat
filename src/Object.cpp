// Class includes
#include "Object.h"

// Own includes
#include "Calc/Vector2.h"

void Object::move(const sf::Vector2f& velocity)
{
    pos += velocity;
}

void Object::move(const float& x, const float& y)
{
    pos.x += x;
    pos.y += y;
}

sf::Vector2f Object::getNormal(const unsigned int &i)
{
    sf::Vector2f p1 = getVertex(i);
    sf::Vector2f p2 = (i + 1) < getVCount() ? getVertex(i + 1) : getVertex(0);

    return Calc::Vector2::perpendicularCW(p2 - p1);
}

sf::Vector2f Object::getVertex(const unsigned int &i)
{
    return shape.getTransform().transformPoint(shape.getPoint(i));
}

unsigned int Object::getVCount()
{
    return shape.getPointCount();
}

unsigned int Object::getN()
{
    return getVCount();
}

const sf::Vector2f &Object::getSize()
{
    return size;
}

void Object::setSize(const sf::Vector2f &size)
{
    this->size = size;
}

void Object::setSize(const float &x, const float &y)
{
    size.x = x;
    size.y = y;
}

const sf::Vector2f &Object::getPos()
{
    return pos;
}

void Object::setPos(const sf::Vector2f &pos)
{
    this->pos = pos;
}

void Object::setPos(const float &x, const float &y)
{
    pos.x = x;
    pos.y = y;
}

const sf::Vector2f &Object::getVelocity()
{
    return velocity;
}

void Object::setVelocity(const sf::Vector2f &velocity)
{
    this->velocity = velocity;
}

void Object::setVelocity(const float &x, const float &y)
{
    velocity.x = x;
    velocity.y = y;
}

const sf::Vector2f &Object::getV()
{
    return V;
}

void Object::setV(const sf::Vector2f &V)
{
    this->V = V;
}

void Object::setV(const float &x, const float &y)
{
    V.x = x;
    V.y = y;
}

sf::ConvexShape &Object::getShape()
{
    return shape;
}
