// Class includes
#include "SAT/NaiveSAT.h"

// Own includes
#include "Calc/Vector2.h"
#include "Object.h"

void NaiveSAT::computeInterval(Object &C, const sf::Vector2f &D, float &min, float &max)
{
    min = Calc::Vector2::dotProduct(D, C.getVertex(0)),
    max = min;

    for(unsigned int i = 1; i < C.getVCount(); ++i) {
        float value = Calc::Vector2::dotProduct(D, C.getVertex(i));

        if(value < min) {
            min = value;
        } else if(value > max) {
            max = value;
        }
    }
}

bool NaiveSAT::noIntersect(const float &min0, const float &max0, const float &min1, const float &max1)
{
    return (min0 > max1) || (min1 > max0);
}

bool NaiveSAT::testIntersection(Object &C0, Object &C1)
{
    for(unsigned int i = 0; i < C0.getN(); ++i) {
        sf::Vector2f D = C0.getNormal(i);

        computeInterval(C0, D, min0, max0);
        computeInterval(C1, D, min1, max1);

        if(noIntersect(min0, max0, min1, max1)) {
            return false;
        }
    }

    for(unsigned int i = 0; i < C1.getN(); ++i) {
        sf::Vector2f D = C1.getNormal(i);

        computeInterval(C0, D, min0, max0);
        computeInterval(C1, D, min1, max1);

        if(noIntersect(min0, max0, min1, max1)) {
            return false;
        }
    }

    return true;
}

ContactManifold &NaiveSAT::getManifold()
{
    return manifold;
}
