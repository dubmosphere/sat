// Class includes
#include "SAT/SweptSAT.h"

// Own includes
#include "Calc/Vector2.h"
#include "Object.h"

// STL includes
#include <limits>

void SweptSAT::computeInterval(Object &C, const sf::Vector2f &D, float &min, float &max)
{
    min = Calc::Vector2::dotProduct(D, C.getVertex(0));
    max = min;

    for(unsigned int i = 1; i < C.getVCount(); ++i) {
        float value = Calc::Vector2::dotProduct(D, C.getVertex(i));

        if(value < min) {
            min = value;
        } else if(value > max) {
            max = value;
        }
    }
}

bool SweptSAT::noIntersect(const float &tmax, const float &speed, const float &min0, const float &max0, const float &min1, const float &max1, float &tfirst, float &tlast)
{
    if(max1 < min0) {
        if(speed <= 0.f)
            return true;

        float t = (min0 - max1) / speed;

        if(t > tfirst)
            tfirst = t;

        if(tfirst > tmax)
            return true;

        t = (max0 - min1) / speed;

        if(t < tlast)
            tlast = t;

        if(tfirst > tlast)
            return true;
    } else if(max0 < min1) {
        if(speed >= 0.f)
            return true;

        float t = (max0 - min1) / speed;

        if(t > tfirst)
            tfirst = t;

        if(tfirst > tmax)
            return true;

        t = (min0 - max1) / speed;

        if(t < tlast)
            tlast = t;

        if(tfirst > tlast)
            return true;
    } else {
        if(speed > 0.f) {
            float t = (max0 - min1) / speed;

            if(t < tlast)
                tlast = t;

            if(tfirst > tlast)
                return true;
        } else if(speed < 0.f) {
            float t = (min0 - max1) / speed;

            if(t < tlast)
                tlast = t;

            if(tfirst > tlast)
                return true;
        }
    }

    return false;
}

bool SweptSAT::testIntersection(Object &C0, Object &C1, const float &tmax, float &tfirst, float &tlast)
{
    sf::Vector2f V = C1.getV() - C0.getV();

    tfirst = 0.f;
    tlast = std::numeric_limits<float>::infinity();

    for(unsigned int i = 0; i < C0.getN(); ++i) {
        sf::Vector2f D = C0.getNormal(i);

        computeInterval(C0, D, min0, max0);
        computeInterval(C1, D, min1, max1);

        float speed = Calc::Vector2::dotProduct(D, V);

        if(noIntersect(tmax, speed, min0, max0, min1, max1, tfirst, tlast)) {
            return false;
        }
    }

    for(unsigned int i = 0; i < C1.getN(); ++i) {
        sf::Vector2f D = C1.getNormal(i);

        computeInterval(C0, D, min0, max0);
        computeInterval(C1, D, min1, max1);

        float speed = Calc::Vector2::dotProduct(D, V);

        if(noIntersect(tmax, speed, min0, max0, min1, max1, tfirst, tlast)) {
            return false;
        }
    }

    return true;
}

ContactManifold &SweptSAT::getManifold()
{
    return manifold;
}
