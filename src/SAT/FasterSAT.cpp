// Class includes
#include "SAT/FasterSAT.h"

// Own includes
#include "Calc/Vector2.h"
#include "Object.h"

int FasterSAT::whichSide(Object &C, const sf::Vector2f &P, const sf::Vector2f &D)
{
    posCount = 0;
    negCount = 0;
    zeroCount = 0;

    for(unsigned int i = 0; i < C.getN(); ++i) {
        float t = Calc::Vector2::dotProduct(D, C.getVertex(i) - P);

        if(t > 0.f)
            ++posCount;
        else if(t < 0.f)
            ++negCount;
        else
            ++zeroCount;

        if((posCount > 0 && negCount > 0) || zeroCount > 0)
            return 0;
    }

    return posCount > 0 ? 1 : -1;
}

bool FasterSAT::testIntersection(Object &C0, Object &C1)
{
    for(unsigned int i0 = C0.getN() - 1, i1 = 0; i1 < C0.getN(); i0 = i1++) {
        sf::Vector2f P = C0.getVertex(i1);
        sf::Vector2f D = -C0.getNormal(i0);

        if(whichSide(C1, P, D) > 0) {
            return false;
        }
    }

    for(unsigned int i0 = C1.getN() - 1, i1 = 0; i1 < C1.getN(); i0 = i1++) {
        sf::Vector2f P = C1.getVertex(i1);
        sf::Vector2f D = -C1.getNormal(i0);

        if(whichSide(C0, P, D) > 0) {
            return false;
        }
    }

    return true;
}

ContactManifold &FasterSAT::getManifold()
{
    return manifold;
}
