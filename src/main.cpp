// Own includes
#include "Object.h"

// SAT includes
#include "SAT/SweptSAT.h"
#include "SAT/FasterSAT.h"
#include "SAT/NaiveSAT.h"

// SFML includes
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

int main(int argc, char **argv)
{
    sf::ContextSettings settings;
    settings.antialiasingLevel = 16;

    sf::RenderWindow window(sf::VideoMode(800, 600, 32), "SAT Collision Detection", sf::Style::Close, settings);
    window.setFramerateLimit(60);

    Object player;
    sf::ConvexShape &playerShape = player.getShape();

    player.setSize(150.f, 100.f);
    player.setPos(125.f, 100.f);
    player.setVelocity(200.f, 200.f);
    /*playerShape.setPointCount(4);
    playerShape.setPoint(0, sf::Vector2f(0.f, 0.f));
    playerShape.setPoint(1, sf::Vector2f(0.f, 100.f));
    playerShape.setPoint(2, sf::Vector2f(100.f, 100.f));
    playerShape.setPoint(3, sf::Vector2f(100.f, 0.f));*/
    playerShape.setPointCount(6);
    playerShape.setPoint(0, sf::Vector2f(50.f, 0.f));
    playerShape.setPoint(1, sf::Vector2f(0.f, 50.f));
    playerShape.setPoint(2, sf::Vector2f(50.f, 100.f));
    playerShape.setPoint(3, sf::Vector2f(100.f, 100.f));
    playerShape.setPoint(4, sf::Vector2f(150.f, 50.f));
    playerShape.setPoint(5, sf::Vector2f(100.f, 0.f));
    playerShape.setFillColor(sf::Color::Green);
    playerShape.setOrigin(player.getSize() / 2.f);
    playerShape.setPosition(player.getPos());

    Object block;
    sf::ConvexShape &blockShape = block.getShape();

    block.setSize(150.f, 100.f);
    block.setPos(400.f, 300.f);
    block.setVelocity(50.f, 0.f);
    /*blockShape.setPointCount(3);
    blockShape.setPoint(0, sf::Vector2f(0.f, 0.f));
    blockShape.setPoint(1, sf::Vector2f(0.f, 100.f));
    blockShape.setPoint(2, sf::Vector2f(100.f, 100.f));*/
    //blockShape.setPoint(3, sf::Vector2f(100.f, 0.f));
    blockShape.setPointCount(6);
    blockShape.setPoint(0, sf::Vector2f(50.f, 0.f));
    blockShape.setPoint(1, sf::Vector2f(0.f, 50.f));
    blockShape.setPoint(2, sf::Vector2f(50.f, 100.f));
    blockShape.setPoint(3, sf::Vector2f(100.f, 100.f));
    blockShape.setPoint(4, sf::Vector2f(150.f, 50.f));
    blockShape.setPoint(5, sf::Vector2f(100.f, 0.f));
    blockShape.setFillColor(sf::Color::Blue);
    blockShape.setOrigin(block.getSize() / 2.f);
    blockShape.setPosition(block.getPos());

    const sf::Time tpf = sf::seconds(1.f / 60.f);
    sf::Time accumulator = sf::Time::Zero;
    sf::Clock clock;

    //NaiveSAT sat; // Naive SAT: Simplest and slowest SAT implementation
    //FasterSAT sat; // Faster SAT: A faster SAT implementation for non- or slow-moving objects
    SweptSAT sat; // Swept SAT: A implementation of SAT for moving objects (based on the simplest SAT implementation, so it is slow)

    while(window.isOpen()) {
        accumulator += clock.restart();

        while(accumulator >= tpf) {
            sf::Event event;

            while(window.pollEvent(event)) {
                switch(event.type) {
                    case sf::Event::Closed:
                        window.close();
                        break;
                    default:
                        break;
                }
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::E))
                playerShape.rotate(100.f * tpf.asSeconds());

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
                playerShape.rotate(-100.f * tpf.asSeconds());

            player.setV(0.f, 0.f);

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
                player.setV(player.getV().x, player.getV().y - player.getVelocity().y);
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
                player.setV(player.getV().x, player.getV().y + player.getVelocity().y);
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
                player.setV(player.getV().x - player.getVelocity().x, player.getV().y);
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
                player.setV(player.getV().x + player.getVelocity().x, player.getV().y);

            player.setV(player.getV() * tpf.asSeconds());
            player.move(player.getV());

            float tfirst = 0.f;
            float tlast = 0.f;

            bool colliding = sat.testIntersection(player, block, tpf.asSeconds(), tfirst, tlast); // Swept SAT Collision for moving objects

            //bool colliding = sat.testIntersection(player, block); // Normal SAT Collision for non- or slow-moving objects (NaiveSAT or FasterSAT)

            if(colliding)
                blockShape.setFillColor(sf::Color::Red);
            else
                blockShape.setFillColor(sf::Color::Blue);

            playerShape.setPosition(player.getPos());

            accumulator -= tpf;
        }

        window.clear();

        window.draw(blockShape);
        window.draw(playerShape);

        window.display();
    }

    return 0;
}
