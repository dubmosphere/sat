#ifndef OBJECT_H
#define OBJECT_H

// SFML includes
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/ConvexShape.hpp>

class Object
{
    public:
        void move(const sf::Vector2f &velocity);
        void move(const float &x, const float &y);

        sf::Vector2f getNormal(const unsigned int &i);
        sf::Vector2f getVertex(const unsigned int &i);
        unsigned int getVCount();
        unsigned int getN();

        const sf::Vector2f &getSize();
        void setSize(const sf::Vector2f &size);
        void setSize(const float &x, const float &y);

        const sf::Vector2f &getPos();
        void setPos(const sf::Vector2f &pos);
        void setPos(const float &x, const float &y);

        const sf::Vector2f &getVelocity();
        void setVelocity(const sf::Vector2f &velocity);
        void setVelocity(const float &x, const float &y);

        const sf::Vector2f &getV();
        void setV(const sf::Vector2f &V);
        void setV(const float &x, const float &y);

        sf::ConvexShape &getShape();

    private:
        sf::Vector2f size;
        sf::Vector2f pos;
        sf::Vector2f velocity;
        sf::Vector2f V;
        sf::ConvexShape shape;
};

#endif // OBJECT_H
