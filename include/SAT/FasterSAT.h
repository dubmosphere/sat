#ifndef FASTERSAT_H
#define FASTERSAT_H

// Own includes
#include "ContactManifold.h"

class Object;

class FasterSAT
{
    public:
        bool testIntersection(Object &C0, Object &C1);

        ContactManifold &getManifold();
    private:
        int whichSide(Object &C, const sf::Vector2f &P, const sf::Vector2f &D);

        int posCount;
        int negCount;
        int zeroCount;

        ContactManifold manifold;
};

#endif // FASTERSAT_H
