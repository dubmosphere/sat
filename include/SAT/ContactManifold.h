#ifndef CONTACTMANIFOLD_H
#define CONTACTMANIFOLD_H

// SFML includes
#include <SFML/System/Vector2.hpp>

// STL includes
#include <vector>

typedef struct
{
    int pointCount;
    std::vector<sf::Vector2f> points;
    sf::Vector2f normal;
} ContactManifold;

#endif // CONTACTMANIFOLD_H
