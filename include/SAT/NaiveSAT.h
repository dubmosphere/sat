#ifndef NAIVESAT_H
#define NAIVESAT_H

// Own includes
#include "ContactManifold.h"

class Object;

class NaiveSAT
{
    public:
        bool testIntersection(Object &C0, Object &C1);

        ContactManifold &getManifold();
    private:
        void computeInterval(Object &C, const sf::Vector2f &D, float &min, float &max);
        bool noIntersect(const float &min0, const float &max0, const float &min1, const float &max1);

        float min0;
        float max0;
        float min1;
        float max1;

        ContactManifold manifold;
};

#endif // NAIVESAT_H
