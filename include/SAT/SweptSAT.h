#ifndef SWEPTSAT_H
#define SWEPTSAT_H

// Own includes
#include "ContactManifold.h"

class Object;

// TODO: Add calculation of contact manifold
// TODO: Add collision response
class SweptSAT
{
    public:
        bool testIntersection(Object &C0, Object &C1, const float &tmax, float &tfirst, float &tlast);

        ContactManifold &getManifold();
    private:
        void computeInterval(Object &C, const sf::Vector2f &D, float &min, float &max);
        bool noIntersect(const float &tmax, const float &speed, const float &min0, const float &max0, const float &min1, const float &max1, float &tfirst, float &tlast);

        float min0;
        float max0;
        float min1;
        float max1;

        ContactManifold manifold;
};

#endif // SWEPTSAT_H
