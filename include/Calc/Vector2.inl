template<class T>
T Calc::Vector2::magnitude(const sf::Vector2<T> &a)
{
    T sqrMag = sqrMagnitude(a);

    return std::sqrt(sqrMag);
}

template<class T>
T Calc::Vector2::sqrMagnitude(const sf::Vector2<T> &a)
{
    return dotProduct(a, a);
}

template<class T>
sf::Vector2<T> Calc::Vector2::perpendicularCW(const sf::Vector2<T> &a) {
    return sf::Vector2<T>(a.y, -a.x);
}

template<class T>
sf::Vector2<T> Calc::Vector2::perpendicularCCW(const sf::Vector2<T> &a) {
    return sf::Vector2<T>(-a.y, a.x);
}

template<class T>
sf::Vector2<T> Calc::Vector2::normalize(const sf::Vector2<T> &a)
{
    sf::Vector2<T> result;

    T m = magnitude(a);

    if(m != 0) {
        result.x = a.x / m;
        result.y = a.y / m;
    }

    return result;
}

template<class T>
sf::Vector2<T> Calc::Vector2::rotate(const sf::Vector2<T> &a, T angle)
{
    return sf::Vector2<T>(
        a.x * std::cos(angle) - a.y * std::sin(angle),
        a.x * std::sin(angle) + a.y * std::cos(angle)
    );
}

template<class T>
sf::Vector2<T> Calc::Vector2::rotate(const sf::Vector2<T> &center, const sf::Vector2<T> &a, T angle)
{
    return sf::Vector2<T>(
        center.x + (a.x - center.x) * std::cos(angle) + (a.y - center.y) * std::sin(angle),
        center.y - (a.x - center.x) * std::sin(angle) + (a.y - center.y) * std::cos(angle)
    );
}

template<class T>
T Calc::Vector2::dotProduct(const sf::Vector2<T> &a, const sf::Vector2<T> &b)
{
    return a.x * b.x + a.y * b.y;
}

// Standard method for cross product
template<class T>
T Calc::Vector2::crossProduct(const sf::Vector2<T> &a, const sf::Vector2<T> &b)
{
    return a.x * b.y - a.y * b.x;
}

// The first of the two i don't know methods of the cross product
template<class T>
sf::Vector2<T> Calc::Vector2::crossProduct(const sf::Vector2<T> &a, T s)
{
    return sf::Vector2<T>(s * a.y, -s * a.x);
}

// The second of the two i don't know methods of the cross product
template<class T>
sf::Vector2<T> Calc::Vector2::crossProduct(T s, const sf::Vector2<T> &a)
{
    return sf::Vector2<T>(-s * a.y, s * a.x);
}
