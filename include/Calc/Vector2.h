#ifndef CALC_VECTOR2_H
#define CALC_VECTOR2_H

#include <cmath>
#include <SFML/System/Vector2.hpp>

namespace Calc {
    namespace Vector2
    {
        template<class T>
        T magnitude(const sf::Vector2<T> &a);

        template<class T>
        T sqrMagnitude(const sf::Vector2<T> &a);

        template<class T>
        sf::Vector2<T> perpendicularCW(const sf::Vector2<T> &a);

        template<class T>
        sf::Vector2<T> perpendicularCCW(const sf::Vector2<T> &a);

        template<class T>
        sf::Vector2<T> normalize(const sf::Vector2<T> &a);

        template<class T>
        sf::Vector2<T> rotate(const sf::Vector2<T> &a, T angle);

        template<class T>
        sf::Vector2<T> rotate(const sf::Vector2<T> &center, const sf::Vector2<T> &a, T angle);

        template<class T>
        T dotProduct(const sf::Vector2<T> &a, const sf::Vector2<T> &b);

        template<class T>
        T crossProduct(const sf::Vector2<T> &a, const sf::Vector2<T> &b);

        template<class T>
        sf::Vector2<T> crossProduct(const sf::Vector2<T> &a, T s);

        template<class T>
        sf::Vector2<T> crossProduct(T s, const sf::Vector2<T> &a);
    }
}

#include "Vector2.inl"

#endif // CALC_VECTOR2_H
