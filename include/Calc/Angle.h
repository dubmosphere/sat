#ifndef CALC_ANGLE_H
#define CALC_ANGLE_H

#include <cmath>

#define PI 3.141592653589793238462643383

namespace Calc
{
    namespace Angle {
        template<class T>
        T radToDeg(T rad);

        template<class T>
        T degToRad(T deg);
    }
}

#include "Angle.inl"

#endif // CALC_ANGLE_H
