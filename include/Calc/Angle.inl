template<class T>
T Calc::Angle::radToDeg(T rad)
{
    return rad * (180 / PI);
}

template<class T>
T Calc::Angle::degToRad(T deg)
{
    return deg * (PI / 180);
}
